package net.freelancertech.base.view.controller;

/**
 * Classe regroupant les propriétés et fonctionnalités commune à tous les
 * managedbeans.
 *
 * @author NGUETSOP
 *
 */
public abstract class AbstractController {

    protected String facesRedirect = "?faces-redirect=true";

    public abstract void initEntity();

    public String create() {
        initEntity();

        return "create" + facesRedirect;
    }

    public String view() {
        return "view" + facesRedirect;
    }

    public String edit() {
        return "edit" + facesRedirect;
    }

    public String delete() {
        return "delete" + facesRedirect;
    }

    public abstract String performCreate();

    public abstract String performEdit();

    public abstract String performDelete();
}
