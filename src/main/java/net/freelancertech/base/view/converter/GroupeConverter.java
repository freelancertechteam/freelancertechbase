package net.freelancertech.base.view.converter;

import java.util.Optional;
import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.service.api.GroupeService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

/**
 * @author auguste
 */
@Named
@ApplicationScoped
@FacesConverter(forClass = Groupe.class)
public class GroupeConverter implements Converter {

    @Autowired
    private GroupeService groupeService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Optional<Groupe> optionalGroupe = groupeService.findByLibelle(value);
        return optionalGroupe.isPresent() ? optionalGroupe.get() : null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Groupe groupe;
        if (value instanceof Groupe) {
            groupe = (Groupe) value;
            return groupe.getLibelle();
        }
        return "";
    }
}
