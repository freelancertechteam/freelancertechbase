/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.view.util;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import net.freelancertech.base.model.entites.PersistentEntity;

/**
 *
 * @author sngos
 */
public class FacesUtils {

    /**
     *
     * @param entities
     * @param none
     * @return
     */
    public static SelectItem[] getSelectItems(List<? extends PersistentEntity> entities, boolean none) {
        int size = none ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (none) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        
        for (PersistentEntity entity : entities) {
            items[i++] = new SelectItem(entity, entity.toString());
        }
        return items;
    }

    public static void addInfo(final String sommaire, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, sommaire, detail));
    }

    public static void addWarn(String sommaire, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, sommaire, detail));
    }

    public static void addError(String sommaire, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, sommaire, detail));
    }

    public static void addError(String sommaire) {
        addError(sommaire, "");
    }

    public static void addError(Class clazz, Exception exception, String sommaire, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, sommaire, detail));
    }

    public static void addError(Class clazz, Exception exception, String sommaire) {
        addError(sommaire, "");
    }

    public static void addFatal(String sommaire, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, sommaire, detail));
    }

    public static void addInfo(String string) {
        addInfo(string, "");
    }
}
