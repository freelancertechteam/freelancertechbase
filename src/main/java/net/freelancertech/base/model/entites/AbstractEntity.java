/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 *
 * @author jnguetsop
 */
@MappedSuperclass
public abstract class AbstractEntity implements PersistentEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    protected Long id;
    @Version
    @Column(name = "version", nullable = false)
    protected Integer version;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_cre", nullable = false)
    protected Date dateCre;
    @Column(name = "user_cre", nullable = false)
    protected String userCre;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_maj", nullable = false)
    protected Date dateMaj;
    @Column(name = "user_maj", nullable = false)
    protected String userMaj;

    @Override
    public Long getId() {
        return this.id;
    }

    @PrePersist
    public void prePersist() {
        setDateCre(new Date());
        setDateMaj(getDateCre());
    }

    @PreUpdate
    public void preUpdate() {
        setDateMaj(new Date());
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getDateCre() {
        return dateCre;
    }

    public void setDateCre(Date dateCre) {
        this.dateCre = dateCre;
    }

    public String getUserCre() {
        return userCre;
    }

    public void setUserCre(String userCre) {
        this.userCre = userCre;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getUserMaj() {
        return userMaj;
    }

    public void setUserMaj(String userMaj) {
        this.userMaj = userMaj;
    }

    @Override
    public boolean isIdSet() {
        return (this.id == null);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AbstractEntity)) {
            return false;
        }
        AbstractEntity other = (AbstractEntity) object;

        if ((this.getId() == null) || (other.getId() == null)) {
            return false;
        }

        return this.getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public abstract String toString();
}
