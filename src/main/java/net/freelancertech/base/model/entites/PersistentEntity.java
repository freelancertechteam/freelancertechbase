/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.io.Serializable;

/**
 *
 * @author jnguetsop
 * @param <T> La clé primaire de l'entité.
 */
public interface PersistentEntity<T extends Serializable> extends Serializable {

    T getId();

    void setId(T id);

    /**
     * Helper method to know whether the primary key is set or not.
     *
     * @return true if the primary key is set, false otherwise
     */
    boolean isIdSet();
}
