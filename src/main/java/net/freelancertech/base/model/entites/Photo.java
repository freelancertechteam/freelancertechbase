/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 *
 * @author NGUETSOP
 */
@Entity
@Table(name = "photo")
public class Photo extends AbstractEntity implements Serializable, Comparable<Photo> {

    private static final long serialVersionUID = 1L;
    @Lob
    @Column(name = "contenu", nullable = false)
    private byte[] contenu;
    @Lob
    @Column(name = "description")
    private String description;
    @Column(name = "type", nullable = false)
    private String type;

    public Photo() {
    }

    public Photo(byte[] contenu, String description, String type) {
        this.contenu = contenu;
        this.description = description;
        this.type = type;
    }

    public byte[] getContenu() {
        return contenu;
    }

    public void setContenu(byte[] contenu) {
        this.contenu = contenu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Photo)) {
            return false;
        }
        Photo other = (Photo) object;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return type + " - " + description;
    }

    @Override
    public int compareTo(Photo other) {
        // Si le parametre est null
        if (other == null) {
            return 1;
        }

        // Si le parametre n'a pas d'ID
        if (other.id == null) {
            return 1;
        }

        // Si l'objet en cours n'a pas d'ID
        if (this.id == null) {
            return -1;
        }

        // Comparaison des IDs
        return this.id.compareTo(other.id);
    }
}
