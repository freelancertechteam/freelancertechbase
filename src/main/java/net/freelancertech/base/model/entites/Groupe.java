/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author User
 */
@Entity
@Table(name = "groupe", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nom"}),
    @UniqueConstraint(columnNames = {"libelle"})})
public class Groupe extends AbstractEntity implements Serializable, Comparable<Groupe> {

    private static final long serialVersionUID = 1L;
    @Column(nullable = false, length = 30)
    private String nom;
    @Column(nullable = false, length = 60)
    private String libelle;
    @Column(length = 255)
    private String description;
    @Column(nullable = false)
    private Boolean actif;
    @Column(name = "parent", length = 60)
    private String parent;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupe", orphanRemoval = true, targetEntity = UtilisateurGroupe.class)
    private List<UtilisateurGroupe> ugs = new ArrayList<>(0);

    public Groupe() {
    }

    public Groupe(String nom) {
        this.nom = nom;
    }

    public Groupe(String nom, String libelle, String description, Boolean actif, String parent) {
        this.nom = nom;
        this.libelle = libelle;
        this.description = description;
        this.actif = actif;
        this.parent = parent;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isActif() {
        return actif;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public List<UtilisateurGroupe> getUgs() {
        return ugs;
    }

    public void setUgs(List<UtilisateurGroupe> ugs) {
        this.ugs = ugs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nom != null ? nom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Groupe)) {
            return false;
        }
        Groupe other = (Groupe) object;

        return Objects.equals(this.nom, other.nom);
    }

    @Override
    public String toString() {
        return nom + " - " + libelle;
    }

    /**
     * @return the parent
     */
    public String getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public int compareTo(Groupe other) {
        // Si le parametre est null
        if (other == null) {
            return 1;
        }

        // Si le parametre n'a pas d'ID
        if (other.id == null) {
            return 1;
        }

        // Si l'objet en cours n'a pas d'ID
        if (this.id == null) {
            return -1;
        }

        // Comparaison des IDs
        return this.id.compareTo(other.id);
    }
}
