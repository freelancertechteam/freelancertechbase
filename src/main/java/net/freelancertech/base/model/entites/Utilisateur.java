/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.model.entites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Simon
 */
@Entity
@Table(name = "utilisateur", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"login"})})
public class Utilisateur extends AbstractEntity implements Serializable, Comparable<Utilisateur> {

    private static final long serialVersionUID = 1L;
    @Column(name = "login", nullable = false, length = 30)
    private String login;
    @Column(name = "nom", nullable = false, length = 60)
    private String nom;
    @Column(name = "pwd", nullable = false, length = 255)
    private String pwd;
    @Column(name = "poste", length = 60)
    private String poste;
    @Column(name = "status", nullable = false)
    private Boolean status;
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;
    @Column(name = "expired", nullable = false)
    private Boolean expired;
    @Column(name = "niveau")
    private Integer niveau = 3;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur", orphanRemoval = true, targetEntity = UtilisateurGroupe.class)
    private List<UtilisateurGroupe> ugs = new ArrayList<>(0);

    public Utilisateur() {
        setUserCre("admin");
        setUserMaj("admin");
    }

    public Utilisateur(String login, String nom, String pwd, String poste, Boolean status, Boolean enabled, Boolean expired) {
        this.login = login;
        this.nom = nom;
        this.pwd = pwd;
        this.poste = poste;
        this.status = status;
        this.enabled = enabled;
        this.expired = expired;

        setUserCre("admin");
        setUserMaj("admin");
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public Boolean getStatus() {
        return status;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getExpired() {
        return expired;
    }

    public Boolean isExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public Integer getNiveau() {
        return niveau;
    }

    public void setNiveau(Integer niveau) {
        this.niveau = niveau;
    }

    public List<UtilisateurGroupe> getUgs() {
        return ugs;
    }

    public void setUgs(List<UtilisateurGroupe> ugs) {
        this.ugs = ugs;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.login);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilisateur other = (Utilisateur) obj;
        return Objects.equals(this.login, other.login);
    }

    @Override
    public String toString() {
        return login + " - " + nom + " - " + poste;
    }

    @Override
    public int compareTo(Utilisateur other) {
        // Si le parametre est null
        if (other == null) {
            return 1;
        }

        // Si le parametre n'a pas d'ID
        if (other.id == null) {
            return 1;
        }

        // Si l'objet en cours n'a pas d'ID
        if (this.id == null) {
            return -1;
        }

        // Comparaison des IDs
        return this.id.compareTo(other.id);
    }
}
