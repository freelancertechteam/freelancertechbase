package net.freelancertech.base.model.repository;

import java.util.List;
import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author NGUETSOP
 */
public interface UtilisateurGroupeRepository extends JpaRepository<UtilisateurGroupe, Long> {

    public List<UtilisateurGroupe> findByGroupe(Groupe groupe);

    public List<UtilisateurGroupe> findByUtilisateur(Utilisateur utilisateur);
}
