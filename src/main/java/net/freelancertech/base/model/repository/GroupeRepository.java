package net.freelancertech.base.model.repository;

import java.util.Optional;
import net.freelancertech.base.model.entites.Groupe;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author NGUETSOP
 */
public interface GroupeRepository extends JpaRepository<Groupe, Long> {

    public Optional<Groupe> findOneByNom(String nom);

    public Optional<Groupe> findOneByLibelle(String libelle);
}
