package net.freelancertech.base.model.repository;

import net.freelancertech.base.model.entites.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author NGUETSOP
 */
public interface PhotoRepository extends JpaRepository<Photo, Long> {
}
