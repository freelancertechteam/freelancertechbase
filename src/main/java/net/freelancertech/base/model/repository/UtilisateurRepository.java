package net.freelancertech.base.model.repository;

import java.util.Optional;
import net.freelancertech.base.model.entites.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author NGUETSOP
 */
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    public Optional<Utilisateur> findOneByLogin(String login);
}
