package net.freelancertech.base.model.service.security;

import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.freelancertech.base.model.repository.UtilisateurGroupeRepository;
import net.freelancertech.base.model.repository.UtilisateurRepository;

/**
 * @author NGUETSOP
 */
@Service
@Transactional(readOnly = true)
public class FreelancerUserDetailsService implements UserDetailsService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private UtilisateurGroupeRepository utilisateurGroupeRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(login);
           // System.out.println("\n\n\n\n\n\n\tLOGIN = " + login + "\n\n\n\n\n\n");

            if (!optionalUtilisateur.isPresent()) {
                throw new Exception("net.freelancertech.base.authentication.usernotfound");
            }

            return new User(optionalUtilisateur.get().getLogin(), optionalUtilisateur.get().getPwd(),
                    optionalUtilisateur.get().isEnabled(), !optionalUtilisateur.get().isExpired(),
                    !optionalUtilisateur.get().isExpired(), optionalUtilisateur.get().isEnabled(),
                    getAuthorities(optionalUtilisateur.get()));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * @param utilisateur
     * @return
     */
    private List<GrantedAuthority> getAuthorities(Utilisateur utilisateur) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<UtilisateurGroupe> ugs = utilisateurGroupeRepository.findByUtilisateur(utilisateur);

        ugs.stream().forEach((ug) -> {
            authorities.add(new SimpleGrantedAuthority(ug.getGroupe().getNom()));
        });

        return authorities;
    }
}
