package net.freelancertech.base.model.service.api;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.entites.Utilisateur;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author jnguetsop
 */
public interface UtilisateurService extends AbstractService<Utilisateur, Long> {

    Optional<Utilisateur> connect(String login, String password) throws Exception;

    Utilisateur save(Utilisateur utilisateur, List<Groupe> groupes) throws Exception;

    Utilisateur update(Utilisateur utilisateur, List<Groupe> groupes) throws Exception;
}
