package net.freelancertech.base.model.service.impl;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.service.api.GroupeService;
import java.util.List;
import java.util.Optional;
import net.freelancertech.base.model.repository.GroupeRepository;
import net.freelancertech.base.model.repository.UtilisateurGroupeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class GroupeServiceImpl extends AbstractServiceImpl<Groupe, Long> implements GroupeService {

    @Autowired
    private GroupeRepository groupeRepository;
    @Autowired
    private UtilisateurGroupeRepository utilisateurGroupeRepository;

    @Override
    public JpaRepository<Groupe, Long> getRepository() {
        return groupeRepository;
    }

    @Override
    public Groupe save(Groupe groupe) throws Exception {
        Optional<Groupe> optionalGroupe = groupeRepository.findOneByNom(groupe.getNom());
        if (optionalGroupe.isPresent()) {
            throw new Exception("Un autre groupe a déjà ce nom : " + groupe.getNom());
        }

        optionalGroupe = groupeRepository.findOneByLibelle(groupe.getLibelle());
        if (optionalGroupe.isPresent()) {
            throw new Exception("Un autre groupe a déjà ce libellé : " + groupe.getLibelle());
        }

        return groupeRepository.save(groupe);
    }

    @Override
    public Groupe update(Groupe groupe) throws Exception {
        Optional<Groupe> optionalGroupe = groupeRepository.findOneByNom(groupe.getNom());

        if ((optionalGroupe.isPresent()) && (!groupe.getId().equals(optionalGroupe.get().getId()))) {
            throw new Exception("Un autre groupe a déjà ce nom : " + groupe.getNom());
        }

        optionalGroupe = groupeRepository.findOneByLibelle(groupe.getLibelle());

        if ((optionalGroupe.isPresent()) && (!groupe.getId().equals(optionalGroupe.get().getId()))) {
            throw new Exception("Un autre groupe a déjà ce libelle : " + groupe.getLibelle());
        }

        return groupeRepository.save(groupe);
    }

    @Override
    public void delete(Groupe groupe) throws Exception {
        List<UtilisateurGroupe> ugs = utilisateurGroupeRepository.findByGroupe(groupe);

        if ((ugs != null) && !ugs.isEmpty()) {
            throw new Exception("Il existe des utilisateurs appartenant à ce groupe.");
        }

        groupeRepository.delete(groupe);
    }

    @Override
    public void deleteById(Long id) throws Exception {
        Groupe groupe = getRepository().findOne(id);
        if (groupe != null) {
            delete(groupe);
        }
    }

    @Override
    public Optional<Groupe> findByLibelle(String libelle) {
        return groupeRepository.findOneByLibelle(libelle);
    }
}
