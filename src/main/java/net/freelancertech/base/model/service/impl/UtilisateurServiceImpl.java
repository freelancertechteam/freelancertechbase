package net.freelancertech.base.model.service.impl;

import net.freelancertech.base.model.entites.Groupe;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.service.api.UtilisateurService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.freelancertech.base.model.repository.GroupeRepository;
import net.freelancertech.base.model.repository.UtilisateurGroupeRepository;
import net.freelancertech.base.model.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UtilisateurServiceImpl extends AbstractServiceImpl<Utilisateur, Long> implements UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private GroupeRepository groupeRepository;
    @Autowired
    private UtilisateurGroupeRepository utilisateurGroupeRepository;

    @Override
    public JpaRepository<Utilisateur, Long> getRepository() {
        return utilisateurRepository;
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur, List<Groupe> groupes) throws Exception {
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(utilisateur.getLogin());
        if (optionalUtilisateur.isPresent()) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder(8);
        utilisateur.setPwd(encoder.encode(utilisateur.getPwd()));

        utilisateur = utilisateurRepository.save(utilisateur);
        for (Groupe groupe : groupes) {
            UtilisateurGroupe ug = new UtilisateurGroupe(Boolean.TRUE, "", utilisateur, groupe);
            utilisateurGroupeRepository.save(ug);
        }

        return utilisateur;
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur) throws Exception {
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(utilisateur.getLogin());
        if (optionalUtilisateur.isPresent()) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder(8);
        utilisateur.setPwd(encoder.encode(utilisateur.getPwd()));

        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) throws Exception {
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(utilisateur.getLogin());

        if ((optionalUtilisateur.isPresent()) && (!utilisateur.getId().equals(optionalUtilisateur.get().getId()))) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur, List<Groupe> groupes) throws Exception {
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(utilisateur.getLogin());

        if ((optionalUtilisateur.isPresent()) && (!utilisateur.getId().equals(optionalUtilisateur.get().getId()))) {
            throw new Exception("Un autre utilisateur a déjà ce login : " + utilisateur.getLogin());
        }

        utilisateur = utilisateurRepository.save(utilisateur);

        List<UtilisateurGroupe> ugs = utilisateurGroupeRepository.findByUtilisateur(utilisateur);
        List<Groupe> gs = new ArrayList<>(ugs.size());

        for (UtilisateurGroupe ug : ugs) {
            gs.add(ug.getGroupe());

            if (!groupes.contains(ug.getGroupe())) {
                utilisateurGroupeRepository.delete(ug);
            }
        }

        for (Groupe groupe : groupes) {
            if (!gs.contains(groupe)) {
                UtilisateurGroupe ug = new UtilisateurGroupe(Boolean.TRUE, "", utilisateur, groupe);
                utilisateurGroupeRepository.save(ug);
            }
        }

        return utilisateur;
    }

    @Override
    public void delete(Utilisateur utilisateur) throws Exception {
        List<UtilisateurGroupe> ugs = utilisateurGroupeRepository.findByUtilisateur(utilisateur);

        ugs.stream().forEach((ug) -> {
            utilisateurGroupeRepository.delete(ug);
        });

        utilisateurRepository.delete(utilisateur);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Utilisateur> connect(String login, String password) throws Exception {
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findOneByLogin(login);

        if ((optionalUtilisateur.isPresent()) && (optionalUtilisateur.get().getPwd().equals(password))) {
            if (!optionalUtilisateur.get().isEnabled()) {
                throw new Exception("Echec de connexion : Votre compte est suspendu.");
            }
        }
        return optionalUtilisateur;
    }

    @Override
    public void deleteById(Long id) throws Exception {
        Utilisateur utilisateur = getRepository().findOne(id);
        if (utilisateur != null) {
            delete(utilisateur);
        }
    }
}
