package net.freelancertech.base.model.service.impl;

import java.util.List;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;
import net.freelancertech.base.model.repository.UtilisateurGroupeRepository;
import net.freelancertech.base.model.service.api.UtilisateurGroupeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jnguetsop
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UtilisateurGroupeServiceImpl extends AbstractServiceImpl<UtilisateurGroupe, Long> implements UtilisateurGroupeService {

    @Autowired
    private UtilisateurGroupeRepository utilisateurGroupeRepository;

    @Override
    public JpaRepository<UtilisateurGroupe, Long> getRepository() {
        return utilisateurGroupeRepository;
    }

    @Override
    public UtilisateurGroupe save(UtilisateurGroupe ug) throws Exception {
        return utilisateurGroupeRepository.save(ug);
    }

    @Override
    public UtilisateurGroupe update(UtilisateurGroupe ug) throws Exception {
        return utilisateurGroupeRepository.save(ug);
    }

    @Override
    public void delete(UtilisateurGroupe ug) throws Exception {
        utilisateurGroupeRepository.delete(ug);
    }

    @Override
    public void deleteById(Long id) throws Exception {
        UtilisateurGroupe ug = getRepository().findOne(id);
        if (ug != null) {
            delete(ug);
        }
    }

    @Override
    public List<UtilisateurGroupe> findByUtilisateur(Utilisateur utilisateur) {
        return utilisateurGroupeRepository.findByUtilisateur(utilisateur);
    }

}
