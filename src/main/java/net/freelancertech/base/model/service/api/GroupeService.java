package net.freelancertech.base.model.service.api;

import java.util.Optional;
import net.freelancertech.base.model.entites.Groupe;

/**
 *
 * @author jnguetsop
 */
public interface GroupeService extends AbstractService<Groupe, Long> {

    public Optional<Groupe> findByLibelle(String libelle);
}
