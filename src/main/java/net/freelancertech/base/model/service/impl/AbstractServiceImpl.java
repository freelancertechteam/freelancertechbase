package net.freelancertech.base.model.service.impl;

import java.io.Serializable;
import java.util.List;
import net.freelancertech.base.model.service.api.AbstractService;

/**
 * Generic service implementation.
 *
 * @author NGUETSOP
 * @param <T>
 * @param <K>
 */
public abstract class AbstractServiceImpl<T extends Serializable, K extends Serializable>
        implements AbstractService<T, K> {

    @Override
    public T find(K id) {
        return getRepository().findOne(id);
    }

    @Override
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Long count() {
        return getRepository().count();
    }
}
