package net.freelancertech.base.model.service.api;

import java.util.List;
import net.freelancertech.base.model.entites.Utilisateur;
import net.freelancertech.base.model.entites.UtilisateurGroupe;

/**
 *
 * @author jnguetsop
 */
public interface UtilisateurGroupeService extends AbstractService<UtilisateurGroupe, Long> {

    public List<UtilisateurGroupe> findByUtilisateur(Utilisateur utilisateur);
}
