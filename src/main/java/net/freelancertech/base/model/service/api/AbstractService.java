package net.freelancertech.base.model.service.api;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author NGUETSOP
 * @param <T> - Entity type
 * @param <K> - Entity's primary key type
 */
public interface AbstractService<T extends Serializable, K extends Serializable> {

    public abstract JpaRepository<T, K> getRepository();

    /**
     * Find entity by id.
     *
     * @param id Id
     * @return Matched entity
     */
    T find(final K id);

    /**
     * Find all entities of type T.
     *
     * @return List of entities
     */
    List<T> findAll();

    /**
     * Save entity.
     *
     * @param entity Entity to save
     * @return
     * @throws java.lang.Exception
     */
    public default T save(T entity) throws Exception {
        return getRepository().save(entity);
    }

    /**
     * Update entity.
     *
     * @param entity Entity to update
     * @return
     * @throws java.lang.Exception
     */
    public default T update(T entity) throws Exception {
        return getRepository().save(entity);
    }

    /**
     * Delete entity.
     *
     * @param entity Entity to delete
     * @throws java.lang.Exception
     */
    public default void delete(T entity) throws Exception {
        getRepository().delete(entity);
    }

    /**
     * Delete entity by id.
     *
     * @param id ID
     * @throws java.lang.Exception
     */
    public default void deleteById(final K id) throws Exception {
        getRepository().delete(id);
    }

    /**
     * Returns number of entities of type T.
     *
     * @return Number of entities
     */
    Long count();
}
