/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author NGUETSOP
 */
@Configuration
@ComponentScan(basePackages = {"net.freelancertech.base"})
public class CoreConfig {
}
