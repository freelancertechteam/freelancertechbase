/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.freelancertech.base.util;

/**
 *<p>From developpez.com</p>
 * <a href="http://oliviercroisier.developpez.com/tutoriels/java/java8-nouveautes-interfaces">liens</a>
 * @author auguste
 * @param <T>
 */
public interface Orderable<T> extends Comparable<T> {
 
    // La méthode compareTo() est définie
    // dans la super-interface Comparable
 
    public default boolean isAfter(T other) {
        return compareTo(other) > 0;
    }
 
    public default boolean isBefore(T other) {
        return compareTo(other) < 0;
    }
 
    public default boolean isSameAs(T other) {
        return compareTo(other) == 0;
    }
    
}
