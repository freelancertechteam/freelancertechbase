/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.util;

/**
 *
 * @author NGUETSOP
 */
public class ExceptionUtil {

    public static String getRootErrorMessage(Exception exception,
            String defaultMessage) {
        // Default to general error message that registration failed.
        // String errorMessage =
        // "Registration failed. See server log for more information";
        if (exception == null) {
            // This shouldn't happen, but return the default messages
            return defaultMessage;
        }

        // Start with the exception and recurse to find the root cause
        Throwable t = exception;
        while (t != null) {
            // Get the message from the Throwable class instance
            defaultMessage = t.getLocalizedMessage();
            t = t.getCause();
        }

        // This is the root cause message
        return defaultMessage;
    }
}
