/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.util;

import java.io.File;

/**
 *
 * @author NGUETSOP
 */
public interface Constantes {

    /**
     * Longueur du matricule d'un étudiant
     */
    final Integer L_MATRICULE = 6;
    final String REPORTS_REPOSITORY = "resources" + File.separator + "reports";
    final String IMAGES_REPOSITORY = "resources" + File.separator + "images";
    final String RESOURCE_REPOSITORY = "resources" + File.separator + "i18n";
    final String BUNDLE_NAME = "FreelancerResourceBundle";
    /**
     * Fichier de configuration
     */
    final String FICHIER_DE_CONFIGURATION = "freelancer.config";
    final String FICHIER_PDF = "out.pdf";
    final String APPLICATION_NAME = "FreelancerTechBase";
}
