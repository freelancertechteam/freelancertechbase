/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.freelancertech.base.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


/**
 *
 * @author mkwedi
 */
public class LogManager {
    private static final Map<Class, Logger> _logPool =
            new HashMap<>();
    
    private static synchronized Logger getLog(Class clazz){
        Logger log = _logPool.get(clazz);
        if( log == null ){
            log = Logger.getLogger(clazz.getName());
            _logPool.put(clazz, log);
        }
        return log;
    }
    
    public static synchronized void info(Object instance, String msg){
        Logger log = getLog(instance.getClass());
        log.info(msg);
    }
    
    public static synchronized void debug(Object instance, String msg){
        Logger log = getLog(instance.getClass());
        log.info(msg);//todo:change this to .debug
    }
    
    public static synchronized void fatal(Object instance, String msg){
        Logger log = getLog(instance.getClass());
        log.severe(msg);
    }
    
    public static synchronized void warn(Object instance, String msg){
        Logger log = getLog(instance.getClass());
        log.warning(msg);
    }
    
    public static synchronized void error(Object instance, String msg){
        Logger log = getLog(instance.getClass());
        log.severe(msg);
    }
     public static synchronized void error(Object instance, String msg,Throwable  th){
       Logger log = getLog(instance.getClass());
        log.severe(msg);
    }
}
