/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.freelancertech.base.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Simon
 */
public class ZipUtils {

    private static final int BUFFER_SIZE = 8 * 1024;
    public static final String NATUREPRODUITS = "NATUREPRODUITS";
    public static final String ENTREPRISE = "ENTREPRISE";
    public static final String LOCALITE = "LOCALITE";
    public static final String ESSENCE = "ESSENCE";
    public static final String ENTREPRISE_LOCALITE = "ENTREPRISE_LOCALITE";
    public static final String DETAIL_FICHE = "DETAIL_FICHE";
    public static final String DESTINATION = "DESTINATION";
    public static final String COLIS = "COLIS";
    public static final String LVD = "LVD";
    public static final String EXERCICE = "EXERCICE";
    public static final String UTILISATEUR = "UTILISATEUR";
    public static final String USERGROUPE = "USERGROUPE";

    /**
     * Zippe récursivement un répertoire, en mettant les chemins de fichiers en
     * relatif.
     * <p>
     * Les accents des noms de fichiers sont supprimés, voir
     * {@link OutilsString#sansAccents(String)} pour plus de détails.
     *
     * @param sources
     * @param destination Le flux du fichier zip résultat
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void zipFile(OutputStream destination, Map<String, InputStream> sources)
            throws FileNotFoundException, IOException {
        //Ajout du checksum : Adler32 (plus rapide) ou CRC32
        try (CheckedOutputStream checksum = new CheckedOutputStream(destination, new Adler32());
                BufferedOutputStream bos = new BufferedOutputStream(checksum, BUFFER_SIZE);
                ZipOutputStream zos = new ZipOutputStream(bos);) {
                //Création d'un buffer de sortie afin d'améliorer les performances               

            //Création d'un flux d'écriture Zip vers le fichier à travers le buffer
            //Compression maximale
            try {
                zos.setMethod(ZipOutputStream.DEFLATED);
                zos.setLevel(Deflater.BEST_COMPRESSION);
            } catch (Exception ignor) {
            }
            zipFileSample(sources, zos);

        }
    }

    /**
     * Zippe récursivement un répertoire, en mettant les chemins de fichiers en
     * relatif.
     * <p>
     * Les accents des noms de fichiers sont supprimés, voir
     * {@link OutilsString#sansAccents(String)} pour plus de détails.
     *
     * @param dirsource Le répertoire à compresser
     * @param zipdest Le nom du fichier zip résultat
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void zipDir(String dirsource, String zipdest)
            throws FileNotFoundException, IOException {

        //Création d'un flux d'écriture vers un fichier
        FileOutputStream fos = new FileOutputStream(zipdest);
        try {
            //Ajout du checksum : Adler32 (plus rapide) ou CRC32
            CheckedOutputStream checksum = new CheckedOutputStream(fos, new Adler32());
            try {
                //Création d'un buffer de sortie afin d'améliorer les performances
                BufferedOutputStream bos = new BufferedOutputStream(checksum, BUFFER_SIZE);
                try {
                    //Création d'un flux d'écriture Zip vers le fichier à travers le buffer
                    ZipOutputStream zos = new ZipOutputStream(bos);
                    try {
                        //Compression maximale
                        try {
                            zos.setMethod(ZipOutputStream.DEFLATED);
                            zos.setLevel(Deflater.BEST_COMPRESSION);
                        } catch (Exception ignor) {
                        }
                        zipDir(dirsource, null, zos);
                    } finally {
                        zos.close();
                    }
                } finally {
                    bos.close();
                }
            } finally {
                checksum.close();
            }
        } finally {
            fos.close();
        }
    }

    /**
     * Étant donné un répertoire base (qui n'est pas inclu dans les entrées
     * <tt>ZipEntry</tt>), le répertoire courant à zipper, et le
     * <tt>ZipOutputStream</tt>
     * de sortie, ajoute les fichiers dans le zip, ou s'appelle récursivement
     * pour ajouter un répertoire fils.
     *
     * @param basedir
     * @param currentdir
     * @param zos
     * @throws FileNotFoundException
     * @throws IOException
     * @see http://www.developpez.net/forums/viewtopic.php?p=1724764
     */
    private static void zipFileSample(Map<String, InputStream> mapFis, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        //create a new File object based on the directory we have to zip
        File zipDir = new File("systat");
        //get a listing of the directory content
        byte[] readBuffer = new byte[BUFFER_SIZE];
        int bytesIn = 0;
        //On met pas File.separator, mais "/" parce que c'est le caractère utilisé
        //dans les ZIP.
        String currentdir2 = "systat/";

        File f; 
        ZipEntry anEntry;
        //Create an empty entry with just dir name, like WinZip, so unzipToDir will
        //behave correctly.
        if (currentdir2.length() > 0) {
            anEntry = new ZipEntry(currentdir2);
            zos.putNextEntry(anEntry);
            zos.closeEntry();
        }

        Set<String> keys = mapFis.keySet();
        for (String key : keys) {
          
                try(InputStream fis = mapFis.get(key);
                     BufferedInputStream bis = new BufferedInputStream(fis, BUFFER_SIZE);  ) {
                    //create a new zip entry
                    anEntry = new ZipEntry(currentdir2 + key);
                    anEntry.setTime(new Date().getTime());
                    //place the zip entry in the ZipOutputStream object
                    zos.putNextEntry(anEntry);
                    //now write the content of the file to the ZipOutputStream
                    while ((bytesIn = bis.read(readBuffer, 0, BUFFER_SIZE)) != -1) {
                        zos.write(readBuffer, 0, bytesIn);
                    }
                    zos.closeEntry();
                }        
        }

    }

    /**
     * Étant donné un répertoire base (qui n'est pas inclu dans les entrées
     * <tt>ZipEntry</tt>), le répertoire courant à zipper, et le
     * <tt>ZipOutputStream</tt>
     * de sortie, ajoute les fichiers dans le zip, ou s'appelle récursivement
     * pour ajouter un répertoire fils.
     *
     * @param basedir
     * @param currentdir
     * @param zos
     * @throws FileNotFoundException
     * @throws IOException
     * @see http://www.developpez.net/forums/viewtopic.php?p=1724764
     */
    private static void zipDir(String basedir, String currentdir, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        //create a new File object based on the directory we have to zip
        File zipDir = (currentdir != null) ? new File(basedir, currentdir) : new File(basedir);
        //get a listing of the directory content
        String[] dirList = zipDir.list();
        byte[] readBuffer = new byte[BUFFER_SIZE];
        int bytesIn = 0;
        //On met pas File.separator, mais "/" parce que c'est le caractère utilisé
        //dans les ZIP.
        String currentdir2 = (currentdir != null) ? (currentdir + "/") : "";

        File f;
        FileInputStream fis;
        BufferedInputStream bis;
        ZipEntry anEntry;
        //Create an empty entry with just dir name, like WinZip, so unzipToDir will
        //behave correctly.
        if (currentdir2.length() > 0) {
            anEntry = new ZipEntry(currentdir2);
            zos.putNextEntry(anEntry);
            zos.closeEntry();
        }
        //loop through dirList, and zip the files
        for (int i = 0; i < dirList.length; i++) {
            f = new File(zipDir, dirList[i]);
            if (!f.exists()) {
                continue;
            }
            if (f.isDirectory()) {
                //if the File object is a directory, call this
                //function again to add its content recursively
                zipDir(basedir, currentdir2 + dirList[i], zos);
                continue;
            }
            //if we reached here, the File object f was not a directory
            //create a FileInputStream on top of f
            fis = new FileInputStream(f);
            try {
                bis = new BufferedInputStream(fis, BUFFER_SIZE);
                try {
                    //create a new zip entry
                    anEntry = new ZipEntry(currentdir2 + dirList[i]);
                    anEntry.setTime(f.lastModified());

                    //place the zip entry in the ZipOutputStream object
                    zos.putNextEntry(anEntry);
                    //now write the content of the file to the ZipOutputStream
                    while ((bytesIn = bis.read(readBuffer, 0, BUFFER_SIZE)) != -1) {
                        zos.write(readBuffer, 0, bytesIn);
                    }
                    zos.closeEntry();
                } finally {
                    bis.close();
                }
            } finally {
                //close the Stream
                fis.close();
            }
        }
    }

    private static void copyInputStream(InputStream in, OutputStream out)
            throws IOException {
        byte[] buffer = new byte[1024];
        int len;

        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }

        in.close();
        out.close();
    }

    public static void unzip(File file, String destName) {
        Enumeration entries;
        try(ZipFile zipFile = new ZipFile(file);) { 
            entries = zipFile.entries();
            Path dir=Paths.get(".");
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                if (entry.isDirectory()) {
                     dir=Paths.get(destName,entry.getName());
                    Files.createDirectory(dir);                    
                    continue;
                }
                InputStream is=zipFile.getInputStream(entry);
                Path sfile=Paths.get(dir.getParent().toString(),entry.getName());
                Files.copy(is, sfile);
            }
        } catch (IOException ioe) {
            System.err.println("Unhandled exception:");
            //ioe.printStackTrace();
            return;
        }
    }
    
    
     public static Map<String,String> unzip(File file) {
         
        Enumeration entries;
        byte[] read=null;
        Map<String,String> map=new HashMap<>();
        try(ZipFile zipFile = new ZipFile(file);) { 
            entries = zipFile.entries();
            
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                if (entry.isDirectory()) {                                      
                    continue;
                }
                
                InputStream is=zipFile.getInputStream(entry);
                Path path=Files.createTempFile("sys", "path");
                Files.delete(path);
                Files.copy(is, path);
                read=Files.readAllBytes(path);                
               
                String out=new String(read, Charset.forName("UTF-8"));
                
                String[] val=entry.getName().split("/");
                map.put(val[1], out);
            }
        } catch (Exception ioe) {
            System.err.println("Unhandled exception:");
            //ioe.printStackTrace();
        }
        return map; 
    }
    
     
      public static Map<String,InputStream> unzipStream(File file) {
        Enumeration entries;
        Map<String,InputStream> map=new HashMap<>();
        try(ZipFile zipFile = new ZipFile(file);) { 
            entries = zipFile.entries();
            
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                if (entry.isDirectory()) {                                      
                    continue;
                }
                
                InputStream is=zipFile.getInputStream(entry);
                 Path path=Files.createTempFile("sys", "path");
                Files.delete(path);
                Files.copy(is, path);
                ByteArrayInputStream bais=new ByteArrayInputStream(Files.readAllBytes(path));
                String[] val=entry.getName().split("/");
                map.put(val[1], bais);
            }
        } catch (Exception ioe) {
            System.err.println("Unhandled exception:");
            //ioe.printStackTrace();
        }
        return map; 
    }
      
       public static void zipFileOut(OutputStream destination, Map<String, ByteArrayOutputStream> sources)
            throws FileNotFoundException, IOException {
        //Ajout du checksum : Adler32 (plus rapide) ou CRC32
        try (CheckedOutputStream checksum = new CheckedOutputStream(destination, new Adler32());
                BufferedOutputStream bos = new BufferedOutputStream(checksum, BUFFER_SIZE);
                ZipOutputStream zos = new ZipOutputStream(bos);) {
                //Création d'un buffer de sortie afin d'améliorer les performances               

            //Création d'un flux d'écriture Zip vers le fichier à travers le buffer
            //Compression maximale
            try {
                zos.setMethod(ZipOutputStream.DEFLATED);
                zos.setLevel(Deflater.BEST_COMPRESSION);
            } catch (Exception ignor) {
            }
            zipFileSampleOut(sources, zos);

        }
    }
       
    private static void zipFileSampleOut(Map<String, ByteArrayOutputStream> mapFis, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        //create a new File object based on the directory we have to zip
        File zipDir = new File("systat");

        //On met pas File.separator, mais "/" parce que c'est le caractère utilisé
        //dans les ZIP.
        String currentdir2 = "systat/";

        ZipEntry anEntry;
        //Create an empty entry with just dir name, like WinZip, so unzipToDir will
        //behave correctly.
        if (currentdir2.length() > 0) {
            anEntry = new ZipEntry(currentdir2);
            zos.putNextEntry(anEntry);
            zos.closeEntry();
        }

        Set<String> keys = mapFis.keySet();
        for (String key : keys) {

            try (ByteArrayOutputStream fis = mapFis.get(key);
                    ByteArrayInputStream bis = new ByteArrayInputStream(fis.toByteArray());) {
                //create a new zip entry
                anEntry = new ZipEntry(currentdir2 + key);
                anEntry.setTime(new Date().getTime());
                //place the zip entry in the ZipOutputStream object
                zos.putNextEntry(anEntry);

                //now write the content of the file to the ZipOutputStream
                zos.write(fis.toByteArray());
                zos.closeEntry();
            }
        }

    }

     public static String trim(final String str){
         char[] tab=str.toCharArray();
        StringBuilder sb=new StringBuilder();
        for (char c : tab) {
          if(c!=' '){
            sb.append(c);
        }  
        }
        return sb.toString();
    }
     
    
}
