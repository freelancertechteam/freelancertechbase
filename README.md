![freelancertech.png](https://bitbucket.org/repo/pnB8L7/images/4248261009-freelancertech.png)

# Description #
FREELANCERTECH BASE est notre ( [www.freelancertech.net](http://freelancertech.net) ) socle technique open source pour certain de nos applications web Java EE, architecturée sur trois couches :

*     La couche web : basée sur le Framework JSF avec la librairie de composants graphiques PrimeFaces.
*     La couche métier ou des services : basée sur le Framework Spring (Security, IoC, Transaction ...) .
*     La couche d’accès aux données : basée sur le Framework Hibernate gérant la persistance des objets en base de données relationnelle(MySQL, Oracle, PostgreSQL, SQL Server, …).

Notre socle technique open source FREELANCERTECH BASE, permet au développeur de se concentrer sur les tâches spécifiques à l'application à développer plutôt qu'à des tâches techniques récurrentes telles que :

*     l'architecture de base de l'application
*     la sécurité (authentification et gestion des rôles)
*     l'accès aux données
*     l'internationalisation
*     la journalisation des événements (logging)
*     le paramétrage de l'application
*     ...

L'utilisation de notre socle technique open source FREELANCERTECH BASE permet notamment :

*     de capitaliser le savoir-faire sans "réinventer la roue"
*     d'accroître la productivité des développeurs une fois le socle technique pris en main
*     d'homogénéiser les développements des applications en assurant la réutilisation de composants fiables
*     donc de faciliter la maintenance notamment évolutive des applications


# Considérations générales #
* Pour toutes les entité, les champs user_cre, user_maj, date_cre et date_maj sont déjà gérés, il n'est donc plus nécessaire de les renseigner à nouveau lors de l'enregistrement ou de la mise à jour.
* Pour tous les champs nécessitant un label, renseigner la propriété id
* Pour les labels, utiliser le composant <p:outputLabel/> en mentionnant sa propriété for

# Base de données #
* Créer un utilisateur mysql avec pour login 'bookstock' et pour mot de passe 'bookstock' également, ceci évitera de modifier le fichier PersistentConfig.java à chque soumission.
* Modifier le fichier WEB-INF/jdbc.properties (Vous devez modifier les variables jdbc.url, jdbc.username,jdbc.password en accord avec les paramètres de votre base de données).
* Le fichier SQL de la base de donnée "freelancertechbase.sql" se trouve dans le répertoire "\src\main\resources" des sources du projet.

![freelancertechbasenetbean2.jpg](https://bitbucket.org/repo/pnB8L7/images/3154896590-freelancertechbasenetbean2.jpg)
![freelancertechbase1.jpg](https://bitbucket.org/repo/pnB8L7/images/3714960555-freelancertechbase1.jpg)
![freelancertechbase3.jpg](https://bitbucket.org/repo/pnB8L7/images/427853957-freelancertechbase3.jpg)